# Lab7 - Rollbacks and reliability

### Part 1
```sql
CREATE DATABASE lab;
CREATE TABLE Player (username TEXT UNIQUE, balance INT CHECK(balance >= 0));
CREATE TABLE Shop (product TEXT UNIQUE, in_stock INT CHECK(in_stock >= 0), price INT CHECK (price >= 0));
INSERT INTO Player (username, balance) VALUES ('Alice', 100);
INSERT INTO Player (username, balance) VALUES ('Uncle Bob clean code lol', 200);
INSERT INTO Shop (product, in_stock, price) VALUES ('marshmello', 10, 10);
```

### Part 2
```sql
CREATE TABLE Inventory (
    username TEXT REFERENCES Player(username) NOT NULL,
    product  TEXT REFERENCES Shop(product) NOT NULL,
    amount   INT CHECK (amount >= 0),
    UNIQUE(username, product)
);
```
